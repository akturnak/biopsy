// This function is called onload in the popup code
function getPageDetails(callback) {
    // Inject the content script into the current page
    chrome.tabs.executeScript(null, { file: 'js/content.js' });
    // Perform the callback when a message is received from the content script
    chrome.runtime.onMessage.addListener(function (message) {
        // Call the callback function
        callback(message);
    });
};

function printExcelPage(direct) {
    console.log(direct);
}

function printExcelPage_OLD(direct) {
    /* set up XMLHttpRequest */
    var url = "direction.xlsx";
    var oReq = new XMLHttpRequest();
    oReq.open("GET", url, true);
    oReq.responseType = "arraybuffer";

    oReq.onload = function (e) {
        var arraybuffer = oReq.response;

        /* convert data to binary string */
        var data = new Uint8Array(arraybuffer);
        var arr = new Array();
        for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
        var bstr = arr.join("");

        /* Call XLSX */
        var workbook = XLSX.read(bstr, { type: "binary" });

        fillValues(workbook, direct);

        downloadFile(workbook, direct.person.full_name + ' ' + direct.person.birthdate);
    }

    oReq.send();
}

function fillValues(workbook, direct) {
    /* DO SOMETHING WITH workbook HERE */
    var first_sheet_name = workbook.SheetNames[0];

    /* Get worksheet */
    var worksheet = workbook.Sheets[first_sheet_name];

    department_cell = 'I14';

    var cell = worksheet[department_cell];

    cell.v = direct.department.name;

    var person_cells = { 'full_name': 'B16', 'sex': 'C17', 'birthdate': 'F17', 'oms': 'D18', 'snils': 'I18', 'full_address': 'E19,B20' };

    var person = direct.person;

    var cell = worksheet[person_cells['full_name']];

    cell.v = person.full_name;

    cell = worksheet[person_cells['sex']];

    cell.v = person.sex == 1 ? 'муж. - 1' : 'жен. - 2';

    cell = worksheet[person_cells['birthdate']];

    cell.v = person.birthdate;

    cell = worksheet[person_cells['oms']];

    cell.v = person.oms;

    cell = worksheet[person_cells['snils']];

    cell.v = person.snils;

    /* Разделяем адрес на части для того чтобы уместить в несколько строк */
    var address = person.full_address.split(',');
    /* Получаем адрес ячейки для вставки первого половины адреса */
    cell = worksheet[person_cells['full_address'].split(',')[0]];

    cell.v = address.shift() + address.shift();
    /* Получаем адрес ячейки для вставки второй половины адреса */
    cell = worksheet[person_cells['full_address'].split(',')[1]];
    /* Вставляем оставшуюся часть адреса */
    cell.v = address;

}

function downloadFile(workbook, filename) {
    /* Save file */
    /* bookType can be 'xlsx' or 'xlsm' or 'xlsb' */
    var wopts = { bookType: 'xlsx', bookSST: false, type: 'binary' };

    var wbout = XLSX.write(workbook, wopts);

    function s2ab(s) {
        var buf = new ArrayBuffer(s.length);
        var view = new Uint8Array(buf);
        for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    }

    /* the saveAs call downloads a file on the local machine */
    saveAs(new Blob([s2ab(wbout)], { type: "" }), filename + ".xlsx")
}