// Send a message containing the page details back to the event page
chrome.runtime.sendMessage({
        'firstname': document.querySelectorAll("table[name='FIRSTNAME'] .input-ctrl")[0].value,
        'lastname': document.querySelectorAll("table[name='SURNAME'] .input-ctrl")[0].value,
        'fathername': document.querySelectorAll("table[name='LASTNAME'] .input-ctrl")[0].value,
        'oms': document.querySelectorAll("span[name='POLIS_OMS_SER']")[0].innerHTML + ' ' +
        document.querySelectorAll("span[name='POLIS_OMS_NUM']")[0].innerHTML,
        'birthdate': document.querySelectorAll("div[name='BIRTHDATE'] .input-ctrl")[0].value,
        'sex': parseInt(document.querySelectorAll("div[name='SEX'] input[checked='true']")[0].value),
        'snils': document.querySelectorAll("table[name='SNILS'] .input-ctrl")[0].value,
        'addrs_raion': document.querySelectorAll("span[name='REAL_ADDRS_RAION']")[0].innerHTML,
        'addrs_full': document.querySelectorAll("span[name='REAL_ADDRS_ADR_FULL']")[0].innerHTML
});
