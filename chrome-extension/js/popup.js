var app = angular.module('app', []);

app.controller('DirectController', ['$scope', '$http', function ($scope, $http) {
    $scope.person = {};
    $scope.departments = [
        { id: 0, name: 'Онкологическое отделение № 1' },
        { id: 1, name: 'Онкологическое отделение № 2' },
        { id: 2, name: 'Онкологическое отделение паллиативной мед. помощи' },
        { id: 2, name: 'Дневной стационар № 1 при поликлинике (онкологический)' },
    ];
    $scope.get_full_name = function () {
        return $scope.person.lastname + ' ' + $scope.person.firstname + ' ' + $scope.person.fathername;
    };
    $scope.get_full_address = function () {
        return $scope.person.addrs_full;
    };

    $scope.init = function () {
        chrome.runtime.getBackgroundPage(function (eventPage) {
            // Call the getPageInfo function in the event page, passing in 
            // our onPageDetailsReceived function as the callback. This injects 
            // content.js into the current tab's HTML
            eventPage.getPageDetails(function (person) {
                $scope.person = person;
                $scope.person.full_name = $scope.get_full_name();
                $scope.person.full_address = $scope.get_full_address();
                $scope.$apply();
                console.log(person);
            });
        });
    };

    $scope.print = function () {
        $http.get('http://direction.magonco.ru/api/direction', { responseType: "blob", params: { 'department': $scope.department, 'patient': $scope.person } }).
            then(function successCallback(response) {
                file_name = $scope.person.full_name + ' ' + $scope.person.birthdate;
                saveAs(new Blob([response.data], { type: "" }), file_name + ".xlsx");
            });
    }

}]);