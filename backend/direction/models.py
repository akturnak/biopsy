from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.utils import timezone

from rest_framework.authtoken.models import Token


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

class Department(models.Model):
    name = models.CharField(max_length=250)

class Patient(models.Model): 
    lastname = models.CharField(max_length=100) 
    firstname = models.CharField(max_length=100)
    fathername = models.CharField(max_length=100)
    birth_date = models.DateField(null=True, blank=True)
    sex = models.BooleanField()
    snils = models.CharField(max_length=50)
    oms = models.CharField(max_length=50)
    address = models.TextField()

class Direction(models.Model):
    department = models.ForeignKey(Department)
    patient = models.ForeignKey(Patient)
    created = models.DateTimeField(default=timezone.now)    