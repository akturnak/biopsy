# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '+#p1bnfb*2$922$+)-^a#!fxah9$g6bl8tokbwjba4tt19dh%w'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'direction_db',
	'USER': 'direction_user',
	'PASSWORD': '1234',
	'HOST': 'localhost',
	'PORT': '',
    }
}