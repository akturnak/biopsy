from django.conf.urls import url
from api import views

urlpatterns = [   
    url(r'^direction$', views.print_direction),
    url(r'^auth/access_token$', views.get_token),    
]
