import os
import json

from django.conf import settings
from django.http import HttpResponse
# drf
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
# library for excel files
from openpyxl import load_workbook
from openpyxl.writer.excel import save_virtual_workbook


@api_view(['GET'])
def get_token(request):
    """
    Get access token.
    """
    client_id = request.query_params.get('client_id', None)
    if not guid or not client_id:
        return Response(status=400)
    user = User.objects.create(guid=guid)    
    token = Token.objects.get(user=user)
    return Response({'access_token': token.key})


@api_view(['GET'])
#@authentication_classes([TokenAuthentication])
#@permission_classes([IsAuthenticated])
def print_direction(request):
    """
    Print direction.
    """   
    department = request.query_params.get('department', None)
    patient = request.query_params.get('patient', None)

    if department:
        department = json.loads(department)
    else:
        department = {'name': None}

    if patient:
        patient = json.loads(patient)
    else:
        patient = {}

    # открываем шаблон направления
    file = open(os.path.join(settings.BASE_DIR, 'direction.xlsx'), 'rb')    
    wb = load_workbook(filename=file)

    # получаем страницу направления
    ws = wb.worksheets[2]

    # заполняем данные в направлении
    ws['I12'] = department['name']

    ws['E14'] = patient['full_name']
    ws['D15'] = 'муж' if patient['sex'] else 'жен'
    ws['G15'] = patient['birthdate']
    ws['E16'] = patient['oms']
    ws['J16'] = patient['snils']    
    ws['E17'] = patient['full_address']

    # получаем страницу протокола
    ws = wb.worksheets[0]

    # заполняем данные в протоколе
    ws['I5'] = department['name']

    ws['E7'] = patient['full_name']
    ws['D8'] = 'муж' if patient['sex'] else 'жен'
    ws['G8'] = patient['birthdate']
    ws['E9'] = patient['oms']
    ws['J9'] = patient['snils']    
    ws['E10'] = patient['full_address']
    
   
    return HttpResponse(save_virtual_workbook(wb), content_type='application/vnd.ms-excel')
